import React from 'react';
import { Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function ResetVote({fetchDataUsers}) {

	const resetVote = () => {

			
		Swal.fire({
			  title: 'Are you sure you want reset all votes?',
			  showDenyButton: true,
			  confirmButtonText: 'Yes',
			  denyButtonText: `No`,
		}).then((result) => {
			  if (result.isConfirmed) {
			  	// fetch(`https://gnhs-ssg-online-voting-system.herokuapp.com/candidates/reset_vote`, {
			  	fetch(`https://gnhs-ssg-online-voting-system.herokuapp.com/candidates/reset_vote`, {
				method: 'PUT',
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${localStorage.getItem('accessToken')}`,
					
				}
				}).then(data => {
						Swal.fire({
							title: 'Success',
							icon: 'success',
							text: 'User successfully deleted'
						})
				})
			  } 
		})

			

	}

	return(

		<>
			<Button className= "btnDanger p-2 m-1 mb-3" variant="danger"  onClick={() => resetVote()}>Reset all votes to zero</Button>

				
		</>

		)
}