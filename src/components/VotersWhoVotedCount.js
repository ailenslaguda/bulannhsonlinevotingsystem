import React, { useState, useEffect } from 'react';

export default function VotersWhoVotedCount(){

	const [totalVoter, setTotalVoter] = useState(0)
	const [totalVoterWhoVoted, setTotalVoterWhoVoted] = useState(0)
	
	const count = ()=>{

		fetch(`https://gnhs-ssg-online-voting-system.herokuapp.com/students/getStudentVotingStatus`, {
		// fetch(`https://gnhs-ssg-online-voting-system.herokuapp.com/students/getStudentVotingStatus`, {
		method: 'GET',
		headers: {
			"Content-Type": 'application/json',
			"Authorization": `Bearer ${localStorage.getItem('accessToken')}`}
		})
		.then(res=> res.json())
		.then (data => {
			setTotalVoterWhoVoted(data.totalWhoVoted)
			setTotalVoter(data.totalVoter)
			
		})
	}

	useEffect(()=>{
		count()
		
	}, [])

	return(
		<>
			
			<h5 className="p-2 text-left" id="totalVoter">Total Number of Voters: <strong>{totalVoter}</strong></h5> 
			<h5 className="p-2 text-left" id="totalVoter"> Total Number of Students Who Voted: <strong>{totalVoterWhoVoted}</strong></h5>
		</>
	)

}