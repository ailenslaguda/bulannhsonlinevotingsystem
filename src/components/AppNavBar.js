import React, {useContext} from 'react';
import UserContext from '../UserContext';
// import Image from 'react-bootstrap/Image'
import { Link } from 'react-router-dom';
import {Navbar, Nav} from 'react-bootstrap';



export default function AppNavbar(){

	const {user} = useContext(UserContext);
	// const [user, setUser] = useState(localStorage.getItem("email"));
	// console.log(user);

	 // When the user scrolls down 20px from the top of the document, show the button
    

	return (
			<Navbar expand="lg" sticky="top">
				{/*<img src={require("../images/banner.png")} width="50"/>*/}
				<Navbar.Brand className="ms-2 heading" href="#">BULAN NATIONAL HIGH SCHOOL VOTING SYSTEM</Navbar.Brand>
				<Navbar.Toggle aria-controls="basic-navbar-nav"/>

				<Navbar.Collapse  id="basic-navbar-nav">
					<Nav className="ml-auto">

						{ (user.accessToken !== null) ?
							<>	
								{
									(user.isAdmin === true) ?
										<>
											{/*<Nav.Link as={ Link } to='/account'>My Account</Nav.Link>*/}
											<Nav.Link as={ Link } to='/menu'>Admin Dashboard</Nav.Link>
											<Nav.Link as={ Link } to='/logout'>Logout</Nav.Link>
										</>

									:
										<>
											<Nav.Link as={ Link } to='/logout' className = "heading"><u>Done Voting</u></Nav.Link>		
											<Nav.Link><p id="studentName" className = "heading">Student: "{`${localStorage.getItem('lastName')}, ${localStorage.getItem('firstName')} ${localStorage.getItem('middleName')}` }"</p></Nav.Link>		
										</>
								}
							</>
							:
							<>
								<Nav.Link as={ Link } to='/'>Refresh</Nav.Link>
							</>
						}

					</Nav>
				</Navbar.Collapse>
			</Navbar>

		)
}