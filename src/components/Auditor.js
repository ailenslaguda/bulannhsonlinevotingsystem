import React, { useState, useEffect } from 'react';
import { Container, Button, Col, Row, Card } from 'react-bootstrap';
import Swal from 'sweetalert2';
import Image from 'react-bootstrap/Image';
import {Link,Navigate, useNavigate} from 'react-router-dom';

export default function Auditor() {

	// const { user } = useContext (UserContext);
	const navigate = useNavigate();

	const [allCandidates, setAllCandidates] = useState([]);
	const [candidateName, setCandidateName] = useState([]);
	const [voteStatP, setVoteStatP] = useState(false);
	const [adminStat, setAdminStat] = useState(false);

	let position = ""
	
	const abstain = (e, LRN)=>{
		e.preventDefault();
		
		Swal.fire({
		  title: 'Are you sure you want to skip voting for auditor?',
		  showDenyButton: true,
		  confirmButtonText: 'Yes',
		  denyButtonText: `No`,
		}).then((result) => {
		  if (result.isConfirmed) {
		  	// fetch(`https://gnhs-ssg-online-voting-system.herokuapp.com/students/student_update_vote`,{
		  	fetch(`https://gnhs-ssg-online-voting-system.herokuapp.com/students/student_update_vote`,{
					method: 'PUT',
					headers: {
						'Content-Type': 'application/json',
						'Authorization': `Bearer ${localStorage.getItem('accessToken')}`

					},
					body: JSON.stringify({
						position: "Auditor",
						LRN: localStorage.getItem('LRN')
					})
				})
				.then(res => res.json())
				navigate('/pio')

		    Swal.fire('Saved!', '', 'success')
		  } 
		})
	}

	const vote = (e, LRN) => {
		e.preventDefault();
		
		fetch(`https://gnhs-ssg-online-voting-system.herokuapp.com/candidates/update_vote`,{
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${localStorage.getItem('accessToken')}`

			},
			body: JSON.stringify({
				LRN: LRN
			})
		})
		.then(res => res.json())
		.then( data => {
			console.log(data)

			if(data === true){
				Swal.fire({
					title: "Success!",
					icon: 'success',
					text: `You sucessfully voted for ${candidateName} as Auditor`
				})			
				fetch(`https://gnhs-ssg-online-voting-system.herokuapp.com/students/student_update_vote`,{
					method: 'PUT',
					headers: {
						'Content-Type': 'application/json',
						'Authorization': `Bearer ${localStorage.getItem('accessToken')}`

					},
					body: JSON.stringify({
						position: "Auditor",
						LRN: localStorage.getItem('LRN')
					})
				})
				.then(res => res.json())
				navigate('/pio')

			} else{
				Swal.fire({
					title: "Error!",
					icon: 'error',
					text: 'Something went wrong. Please try again.'
				})	
			}
		})
	}

	const fetchData = () => {
		
		position = "Auditor"
		
		fetch(`https://gnhs-ssg-online-voting-system.herokuapp.com/candidates/${position}/get_candidate`,{
			method: 'GET',
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${localStorage.getItem('accessToken')}`,
					 'Accept': 'application/json'
				}
		})
		.then(res=> res.json())
		.then (data => {	

			const candidatesArr = data.map(candidate => {
			setCandidateName(`${candidate.firstName} ${candidate.middleName} ${candidate.lastName}`)
			return(
				<Col className="pt-3">
					<Row xs={12} md={6}>
						<Card className="courseCard p-3" >
							<Card.Body key={candidate._id}>
								<Card.Title><h3>{ `${candidate.lastName}, ${candidate.firstName} ${candidate.middleName} ` }</h3></Card.Title>
								<br/>				
								<Card.Subtitle> Running for {candidate.position }</Card.Subtitle>
								{/*<Card.Subtitle className="mt-2">Grade {candidate.grade } - { candidate.section }</Card.Subtitle>*/}
								<Card.Subtitle className="mt-2"><Image style={{height:'auto',width:'10%'}}  src={`/${candidate.picPath}`}></Image></Card.Subtitle>
								<Button className="btn mt-3" onClick={e => vote(e, candidate._id)}>Vote</Button>
							</Card.Body>
						</Card>
					</Row>
				</Col>

			)	
		})
	
		setAllCandidates(candidatesArr)



		})	
		
	}

	const fetchDataUser = () => {
		fetch(`https://gnhs-ssg-online-voting-system.herokuapp.com/students/${localStorage.getItem('LRN')}/getStudentData`,{
			method: 'GET',
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${localStorage.getItem('accessToken')}`,
					 'Accept': 'application/json'
				}
			})
		.then(res=> res.json())
		.then (data => {
			setVoteStatP(data.votingStatA)
		})
	}
	useEffect(()=> {
		
		fetchData()
		fetchDataUser()
		setAdminStat(localStorage.getItem('isAdmin'))

	}, [])

return(
		(voteStatP !== true) ?
			(adminStat === true)?
				<Navigate to="/admin_dashboard" />
			:
			<Container>
				<h1 className="text-center p-2 m-3">Vote for Auditor</h1>	
					<Button onClick={e=>abstain(e,localStorage.getItem('LRN'))}>Vote for none</Button>
					{allCandidates}
			</Container>							
		:
			<Container>
					<div>
				        <div className="starsec"></div>
				        <div className="starthird"></div>
				        <div className="starfourth"></div>
				        <div className="starfifth"></div>
				    </div>
				          
				      <section className="error">
				        
				        <div className="error__content">
				          <div className="error__message message">
				            <div className="message__title"><h1 className=" p-2 m-3">You're done voting for auditor </h1>
							<Link to={`/pio`}>
								<Button className="p-1 m-1">
									PROCEED
								</Button>
							</Link>
							<Link to={`/logout`}>
								<Button className="p-1 m-1">
										DONE VOTING
								</Button>
							</Link>
							</div>
				       
				          </div>
				        </div>

				      </section>
			</Container>
	)
}