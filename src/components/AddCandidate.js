import { Button, Modal, Form } from 'react-bootstrap';
import React, { useState } from 'react';
import Swal from 'sweetalert2';
// import ImageUploading from 'react-images-uploading';

export default function AddProduct({ fetchData}) {
	 // const [images, setImages] = React.useState([]);
  	 // const maxNumber = 1;
	//Add state for the forms of product
	const [LRN, setLRN] = useState('');
	// const [firstName, setfirstName] = useState('');
	// const [middleName, setmiddleName] = useState('')
	// const [lastName, setlastName] = useState('')
	// const [section, setSection] = useState('')
	// const [grade, setGrade] = useState('')
	// const [sex, setSex] = useState('')
	const [position, setPosition] = useState('')

	const [picPath, setPicPath] = useState('')
	
	const [firstName1, setfirstName1] = useState('');
	const [middleName1, setmiddleName1] = useState('')
	const [lastName1, setlastName1] = useState('')
	const [section1, setSection1] = useState('')
	const [grade1, setGrade1] = useState('')
	const [sex1, setSex1] = useState('')
	// const [position1, setPosition1] = useState('')
	// const [picPath1, setPicPath1] = useState('')
	
	// const onChangeProd = (imageList, addUpdateIndex) => {
 //    // data for submit
 //    	console.log(imageList, addUpdateIndex);
 //    	setImages(imageList);
 //    	setPhoto(images['data_url'])

 //  	};

	//States for our modals to open/close
	const [showAdd, setShowAdd] = useState(false);

	//Functions to handle opening and closing of our AddCourse Modal
	// const openAdd = () => setShowAdd(true);
	const closeAdd = () => setShowAdd(false)

	const populateData = (e, LRN) => {
			// console.log(`https://gnhs-ssg-online-voting-system.herokuapp.com/students/:${LRN}/getStudentData`)
		e.preventDefault();

		fetch(`https://gnhs-ssg-online-voting-system.herokuapp.com/students/${LRN}/getStudentData`,{
			method: 'GET',
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${localStorage.getItem('accessToken')}`,
					 'Accept': 'application/json'
				}
		})
		.then(res=> res.json())
		.then (data => {	
			setfirstName1(data.firstName);
			setmiddleName1(data.middleName);
			setlastName1(data.lastName);
			setSection1(data.section);
			setGrade1(data.grade);
			setSex1(data.sex);
			// setPosition(data.position);

		})

		setShowAdd(true)
	}

	const addCandidate = (e) => {
		e.preventDefault();

		// fetch('http://localhost:4000/candidates/register_candidate', {
		fetch('https://gnhs-ssg-online-voting-system.herokuapp.com/candidates/register_candidate', {
			method: 'POST',
			headers: {
				"Content-Type": 'application/json',
				"Authorization": `Bearer ${localStorage.getItem('accessToken')}`
			},
			body: JSON.stringify({
				LRN:parseFloat(LRN),
				firstName:firstName1,
				middleName:middleName1,
				lastName:lastName1,
				section:section1,
				grade:grade1,
				sex:sex1,
				position:position,
				picPath:picPath 

			})
		})
		.then(res=> res.json())
		.then (data => {
			if(data){
				Swal.fire({
					title: "Success!",
					icon: 'success',
					text: `${firstName1} sucessfully registered as ${position}`
				})
				closeAdd();
				
			} else{
				Swal.fire({
					title: "Error!",
					icon: 'error',
					text: 'Something went wrong. Please try again'
				})
				closeAdd();
			
			}

			setLRN('');
			setfirstName1('');
			setmiddleName1('');
			setlastName1('');
			setSection1('');
			setGrade1('');
			setSex1('');
			setPosition('');
			setPicPath('');

		})
	}


	// useEffect(()=> {
	// 	addCandidate();
	// 	populateData();
	// },[LRN])




	return(
		<>
			<Form.Group className = "p-2">
				<h5>LRN</h5>
				<Form.Control type="text" value = {LRN} onChange={e => setLRN(e.target.value)}  required/>
			</Form.Group>
			<Button className = "p-2 m-1 mb-3" variant="primary" onClick={e => populateData(e, LRN)}>Search and Add Candidate</Button>
		

			<Modal show={showAdd} onHide={closeAdd}>

				<Form>
					<Modal.Header closeButton>
						<Modal.Title className ="welcome">Enroll Candidate</Modal.Title>
					</Modal.Header>

					<Modal.Body className ="p-3 m-3">
						<Form.Group>
							<Form.Label>LRN</Form.Label>
							<Form.Control type="text" value={LRN} onChange={e => setLRN(e.target.value)} required/>
						</Form.Group>

						<Form.Group>
							<Form.Label>First Name</Form.Label>
							<Form.Control type="text" value={firstName1 || ''} onChange={e => setfirstName1(e.target.value)} required/>
						</Form.Group>

						<Form.Group>
							<Form.Label>Middle Name</Form.Label>
							<Form.Control type="text" value={middleName1} onChange={e => setmiddleName1(e.target.value)} required/>
						</Form.Group>

						<Form.Group>
							<Form.Label>Last Name</Form.Label>
							<Form.Control type="text" value={lastName1 || ''} onChange={e => setlastName1(e.target.value)} required/>
						</Form.Group>

						<Form.Group>
							<Form.Label>Section</Form.Label>
							<Form.Control type="text" value={section1 || ''} onChange={e => setSection1(e.target.value)} required/>
						</Form.Group>

						<Form.Group>
							<Form.Label>Grade</Form.Label>
							<Form.Control
								as="select"	
								type="number" 
								required
								value={grade1 || ''}
								onChange={e => setGrade1(e.target.value)}
							>		
							  <option>Select here</option>
							  <option value="7">7</option>
							  <option value="8">8</option>
							  <option value="9">9</option>
							  <option value="10">10</option>
							  <option value="11">11</option>
							  <option value="12">12</option>
							  disabled
							</Form.Control>
						</Form.Group>

						<Form.Group>
							<Form.Label>Sex</Form.Label>
							<Form.Control
								as="select"	 
								required
								value={sex1 || ''}
								onChange={e => setSex1(e.target.value)}
							
							>		
							  <option>Select here</option>
							  <option value="M">Male</option>
							  <option value="F">Female</option>
							</Form.Control>
						</Form.Group>

						<Form.Group>
							<Form.Label>Position</Form.Label>
							<Form.Control
								as="select"	 
								required
								value={position || ''}
								onChange={e => setPosition(e.target.value)}
							>		
							  <option>Select here</option>
							  <option value="President">President</option>
							  <option value="Vice President">Vice President</option>
							  <option value="Secretary">Secretary</option>
							  <option value="Treasurer">Treasurer</option>
							  <option value="Auditor">Auditor</option>
							  <option value="PIO">PIO</option>
							  <option value="Protocol Officer">Protocol Officer</option>
							  <option value="Grade Level Representative">Grade Level Representative</option>
							</Form.Control>
						</Form.Group>

						
						<Form.Group>
							<Form.Label>Photo File Name:</Form.Label>
							<p>  (Note: It must be saved in the public folder. You can ask your IT admin for assistance)</p>
							<Form.Control type="text" value={picPath} onChange={e => setPicPath(e.target.value)} required/>
						</Form.Group>

					
					</Modal.Body>

					<Modal.Footer>
						<Button variant="secondary" onClick={closeAdd}>Close</Button>
						<Button variant="success" type="submit" onClick={(e)=> addCandidate(e)}>Submit</Button>
					</Modal.Footer>
				</Form>
				
			</Modal>

		</>

		)
}



