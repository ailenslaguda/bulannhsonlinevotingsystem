import React from 'react';

export default	function Footer(){
	return(
		<footer className="footer pt-1 mt-3" expand="lg">
			{/*<span>
					<h6>
					<strong>
						<span>Visit our Dakjuk's FB Page: </span> 
						<span> 	<a href="https://www.facebook.com/dakjuk/" className="fa fa-facebook"> @Dakjuk</a></span>
						<span> and our Grocery store's FB Page </span> 
						<span> <a href="https://www.facebook.com/LagudaGroceryStore/" className="fa fa-facebook"> @LagudaGroceryStore </a></span>
	              	</strong>
	              </h6>               
	        </span>*/}
			<span> Terms of Service </span>
			<span> | </span>
			<span> Privacy Policy </span>
			<span> | </span>
			<span> &copy; 2022 AimblePh. All Rights Reserved. </span>
		</footer>

	)
}