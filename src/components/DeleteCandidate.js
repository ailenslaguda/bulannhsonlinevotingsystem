import React from 'react';
import { Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function DeleteCandidate({id, fetchData}) {

	const deleteCandidate = (LRN) => {
		
		Swal.fire({
			  title: 'Are you sure you want to delete this candidate?',
			  showDenyButton: true,
			  confirmButtonText: 'Yes',
			  denyButtonText: `No`,
		}).then((result) => {
			  if (result.isConfirmed) {
			  	fetch(`https://gnhs-ssg-online-voting-system.herokuapp.com/candidates/${LRN}/delete_candidate`, {
				method: 'DELETE',
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${localStorage.getItem('accessToken')}`,
					
				}
				}).then(data => {
						Swal.fire({
							title: 'Success',
							icon: 'success',
							text: 'User successfully deleted'
						})
						fetchData()
				})
			  } 
		})

			

	}

	return(

		<>
			<Button className= "btnDanger" variant="danger" size="sm" onClick={() => deleteCandidate(id)}>Delete</Button>

				
		</>

		)
}