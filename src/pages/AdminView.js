import React, { useState, useEffect } from 'react';
import { Table } from 'react-bootstrap';
import Tab from 'react-bootstrap/Tab'
import Tabs from 'react-bootstrap/Tabs'
import {Navigate} from 'react-router-dom';
// import Swal from 'sweetalert2';
// import Image from 'react-bootstrap/Image';
import DeleteCandidate from '../components/DeleteCandidate';
import DeleteUser from '../components/DeleteUser';
import AddCandidate from '../components/AddCandidate';
import EditCandidate from '../components/EditCandidate';
import EditStudent from '../components/EditStudent';
import ResetVote from '../components/ResetVote';
import VotersWhoVotedCount from '../components/VotersWhoVotedCount';
// import EditProduct from './EditProduct';
// import ArchiveProduct from './ArchiveProduct';
	// const [position, setPosition] = useState([]);
export default function AdminView() {

	const [courses, setCourses] = useState([])
	const [pres, setPres] = useState([])
	const [sec, setSec] = useState([])
	const [tre, setTre] = useState([])
	const [aud, setAud] = useState([])
	const [pio, setPio] = useState([])
	const [po, setPO] = useState([])
	const [glr, setGlr] = useState([])

	// const [data, setdata] = useState([])
	// const [allP, setAllP] = useState([])
	// const [allVP, setAllVP] = useState([])
	// const [allT, setAllT] = useState([])
	// const [allA, setAllA] = useState([])
	// const [allPIO, setAllPIO] = useState([])
	// const [allPO, setAllPO] = useState([])
	// const [allGLR, setAllGLR] = useState([])
	
	let position=""

	const fetchData = () => {
		position = "Secretary"
		fetch(`https://gnhs-ssg-online-voting-system.herokuapp.com/candidates/${position}/get_candidate_admin`,{
			method: 'GET',
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${localStorage.getItem('accessToken')}`,
					 'Accept': 'application/json'
				}
		})
		.then(res=> res.json())
		.then (data => {	

			const coursesArr = data.map(course => {
				// setCurrPosition(course.position)

				return (
					<tr key={course._id}>
						<td>{course.firstName}</td>
						<td>{course.middleName}</td>
						<td>{course.lastName}</td>
						<td>{course.section}</td>
						<td>{course.grade}</td>
						<td>{course.sex}</td>
						<td>{course.position}</td>
						<td>{course.voteCount}</td>
						
						<td><EditCandidate LRN={course._id} fetchData={fetchData}/></td>
						<td><DeleteCandidate id={course._id} fetchData={fetchData}/></td>
					</tr>

				)
			})
				setSec(coursesArr)
		})
	}

	// const [LRN, setLRN] = useState("")
	// const [vpLRN, setvpLRN] = useState("")
	
	
	const fetchDataVP = () => {
		position = "Vice President"
		fetch(`https://gnhs-ssg-online-voting-system.herokuapp.com/candidates/${position}/get_candidate_admin`,{
			method: 'GET',
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${localStorage.getItem('accessToken')}`,
					 'Accept': 'application/json'
				}
		})
		.then(res=> res.json())
		.then (data => {	

			const coursesArr = data.map(course => {
				// setCurrPosition(course.position)
				// setLRN(course._id)
				return (
					<tr key={course._id}>
						<td>{course.firstName}</td>
						<td>{course.middleName}</td>
						<td>{course.lastName}</td>
						<td>{course.section}</td>
						<td>{course.grade}</td>
						<td>{course.sex}</td>
						<td>{course.position}</td>
						<td>{course.voteCount}</td>
						
						<td><EditCandidate LRN={course._id} fetchData={fetchData}/></td>
						<td><DeleteCandidate id={course._id} fetchData={fetchDataVP}/></td>
					</tr>

				)
			})
				setCourses(coursesArr)
		})
	}

	const fetchDataP = () => {
		position = "President"
		fetch(`https://gnhs-ssg-online-voting-system.herokuapp.com/candidates/${position}/get_candidate_admin`,{
			method: 'GET',
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${localStorage.getItem('accessToken')}`,
					 'Accept': 'application/json'
				}
		})
		.then(res=> res.json())
		.then (data => {	

			const coursesArr = data.map(course => {
				// setCurrPosition(course.position)
				console.log(data)
				return (
					<tr key={course._id}>
						<td>{course.firstName}</td>
						<td>{course.middleName}</td>
						<td>{course.lastName}</td>
						<td>{course.section}</td>
						<td>{course.grade}</td>
						<td>{course.sex}</td>
						<td>{course.position}</td>
						<td>{course.voteCount}</td>
						
						<td><EditCandidate LRN={course._id} fetchData={fetchData}/></td>
						<td><DeleteCandidate id={course._id} fetchData={fetchDataP}/></td>
					</tr>

				)
			})
				setPres(coursesArr)
		})
	}

	
	const fetchDataT = () => {
		position = "Treasurer"
		fetch(`https://gnhs-ssg-online-voting-system.herokuapp.com/candidates/${position}/get_candidate_admin`,{
			method: 'GET',
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${localStorage.getItem('accessToken')}`,
					 'Accept': 'application/json'
				}
		})
		.then(res=> res.json())
		.then (data => {	

			const coursesArr = data.map(course => {
				// setCurrPosition(course.position)

				return (
					<tr key={course._id}>
						<td>{course.firstName}</td>
						<td>{course.middleName}</td>
						<td>{course.lastName}</td>
						<td>{course.section}</td>
						<td>{course.grade}</td>
						<td>{course.sex}</td>
						<td>{course.position}</td>
						<td>{course.voteCount}</td>
						
						<td><EditCandidate LRN={course._id} fetchData={fetchData}/></td>
						<td><DeleteCandidate id={course._id} fetchData={fetchDataT}/></td>
					</tr>

				)
			})
				setTre(coursesArr)
				
		})
	}


	const fetchDataA = () => {
		position = "Auditor"
		fetch(`https://gnhs-ssg-online-voting-system.herokuapp.com/candidates/${position}/get_candidate_admin`,{
			method: 'GET',
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${localStorage.getItem('accessToken')}`,
					 'Accept': 'application/json'
				}
		})
		.then(res=> res.json())
		.then (data => {	

			const coursesArr = data.map(course => {
				// setCurrPosition(course.position)

				return (
					<tr key={course._id}>
						<td>{course.firstName}</td>
						<td>{course.middleName}</td>
						<td>{course.lastName}</td>
						<td>{course.section}</td>
						<td>{course.grade}</td>
						<td>{course.sex}</td>
						<td>{course.position}</td>
						<td>{course.voteCount}</td>
						
						<td><EditCandidate LRN={course._id} fetchData={fetchData}/></td>
						<td><DeleteCandidate id={course._id} fetchData={fetchDataA}/></td>
					</tr>

				)
			})
				
			setAud(coursesArr)
		})
	}


	const fetchDataPIO = () => {
			position = "PIO"
		fetch(`https://gnhs-ssg-online-voting-system.herokuapp.com/candidates/${position}/get_candidate_admin`,{
			method: 'GET',
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${localStorage.getItem('accessToken')}`,
					 'Accept': 'application/json'
				}
		})
		.then(res=> res.json())
		.then (data => {	

			const coursesArr = data.map(course => {
				// setCurrPosition(course.position)

				return (
					<tr key={course._id}>
						<td>{course.firstName}</td>
						<td>{course.middleName}</td>
						<td>{course.lastName}</td>
						<td>{course.section}</td>
						<td>{course.grade}</td>
						<td>{course.sex}</td>
						<td>{course.position}</td>
						<td>{course.voteCount}</td>
						
						<td><EditCandidate LRN={course._id} fetchData={fetchData}/></td>
						<td><DeleteCandidate id={course._id} fetchData={fetchDataPIO}/></td>
					</tr>

				)
			})
				// setCourses(coursesArr)
			setPio(coursesArr)
		})
	}

	const fetchDataPO = () => {
			position = "Protocol Officer"
		fetch(`https://gnhs-ssg-online-voting-system.herokuapp.com/candidates/${position}/get_candidate_admin`,{
			method: 'GET',
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${localStorage.getItem('accessToken')}`,
					 'Accept': 'application/json'
				}
		})
		.then(res=> res.json())
		.then (data => {	

			const coursesArr = data.map(course => {
				// setCurrPosition(course.position)

				return (
					<tr key={course._id}>
						<td>{course.firstName}</td>
						<td>{course.middleName}</td>
						<td>{course.lastName}</td>
						<td>{course.section}</td>
						<td>{course.grade}</td>
						<td>{course.sex}</td>
						<td>{course.position}</td>
						<td>{course.voteCount}</td>
						
						<td><EditCandidate LRN={course._id} fetchData={fetchData}/></td>
						<td><DeleteCandidate id={course._id} fetchData={fetchDataPO}/></td>
					</tr>

				)
			})
			setPO(coursesArr)
		})
	}



	const fetchDataGLR = () => {
			position = "Grade Level Representative"
		fetch(`https://gnhs-ssg-online-voting-system.herokuapp.com/candidates/${position}/get_candidate_admin`,{
			method: 'GET',
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${localStorage.getItem('accessToken')}`,
					 'Accept': 'application/json'
				}
		})
		.then(res=> res.json())
		.then (data => {	

			const coursesArr = data.map(course => {
				// setCurrPosition(course.position)

				return (
					<tr key={course._id}>
						<td>{course.firstName}</td>
						<td>{course.middleName}</td>
						<td>{course.lastName}</td>
						<td>{course.section}</td>
						<td>{course.grade}</td>
						<td>{course.sex}</td>
						<td>{course.position}</td>
						<td>{course.voteCount}</td>
						
						<td><EditCandidate LRN={course._id} fetchData={fetchData}/></td>
						<td><DeleteCandidate id={course._id} fetchData={fetchDataGLR}/></td>
					</tr>

				)
			})
			setGlr(coursesArr)
			console.log(glr)
		})
	}

	const [users,setUsers] = useState([])

	const fetchDataUsers = () => {
			// position = "Grade Level Representative"
		fetch(`https://gnhs-ssg-online-voting-system.herokuapp.com/students/getStudentsData`,{
			method: 'GET',
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${localStorage.getItem('accessToken')}`,
					 'Accept': 'application/json'
				}
		})
		.then(res=> res.json())
		.then (data => {	

			const coursesArr = data.map(course => {
				// setCurrPosition(course.position)

				return (
					<tr key={course._id}>
						<td>{course._id}</td>
						<td>{course.firstName}</td>
						<td>{course.middleName}</td>
						<td>{course.lastName}</td>
						<td>{course.section}</td>
						<td>{course.grade}</td>
						<td>{course.sex}</td>
						
						<td><EditStudent LRN={course._id} fetchData={fetchData}/></td>
						<td><DeleteUser id={course._id} fetchData={fetchDataUsers}/></td>
					</tr>

				)
			})
			setUsers(coursesArr)
			
		})
	}

	// const updateVoteCount = (e) => {
	// 	e.preventDefault();
	// }

	useEffect(()=> {
		
		fetchData()
		fetchDataVP()
		fetchDataP()
		fetchDataPO()
		fetchDataGLR()
		fetchDataA()
		fetchDataT()
		fetchDataPIO()
		fetchDataUsers()


	}, 
	// [fetchData(),
	// 	fetchDataVP(),
	// 	fetchDataP(),
	// 	fetchDataPO(),
	// 	fetchDataGLR(),
	// 	fetchDataA(),
	// 	fetchDataT(),
	// 	fetchDataPIO(),
	// 	fetchDataUsers()
	[])


	return(
			(localStorage.getItem('isAdmin') === true) ?	

				<Navigate to='/president'></Navigate> 
				:
		   		 <div className="text-center my-4">
			   		 
			   		 	<h1 className="welcome">Admin Dashboard</h1>
			   		 	<VotersWhoVotedCount />
						<AddCandidate fetchData={fetchData}/>
						<ResetVote className="p-3 mx-4" fetchData={fetchDataUsers}/>				
					
					<Tabs defaultActiveKey="president" id="left-tabs-example" className="mb-3">
						<Tab eventKey="president" title="President">
						  	<h2>President</h2>
	
							<Table striped bordered hover responsive>
								<thead className="bg-dark text-white">
									<tr className="text-center">
										<th>First Name</th>
										<th>Middle Name</th>
										<th>Last Name</th>
										<th>Section</th>
										<th>Grade</th>
										<th>Sex</th>
										<th>Position</th>
										<th>Vote Count</th>
										<th colSpan='2' className="text-center">Actions</th>
										
									</tr>
								</thead>

								<tbody>
									{pres}
								</tbody>
							</Table>				
						</Tab>
						<Tab eventKey="vp" title="Vice President">
						   		<h2>Vice President</h2>
						   		<Table striped bordered hover responsive>
								<thead className="bg-dark text-white">
									<tr className="text-center">
										<th>First Name</th>
										<th>Middle Name</th>
										<th>Last Name</th>
										<th>Section</th>
										<th>Grade</th>
										<th>Sex</th>
										<th>Position</th>
										<th>Vote Count</th>
										<th colSpan='2' className="text-center">Actions</th>
										
									</tr>
								</thead>

								<tbody>
						   			{courses}
									{/*{pres}*/}
								</tbody>
							</Table>		
						</Tab>
						  <Tab eventKey="secretary" title="Secretary">		   	 
					  		<h2>Secretary</h2>
					  		<Table striped bordered hover responsive>
								<thead className="bg-dark text-white">
									<tr className="text-center">
										<th>First Name</th>
										<th>Middle Name</th>
										<th>Last Name</th>
										<th>Section</th>
										<th>Grade</th>
										<th>Sex</th>
										<th>Position</th>
										<th>Vote Count</th>
										<th colSpan='2' className="text-center">Actions</th>
										
									</tr>
								</thead>

								<tbody>
									{sec}
								</tbody>
							</Table>
					   		
					  	</Tab>
						<Tab eventKey="treasurer" title="Treasurer" >
						  	<h2>Treasurer</h2>
						  	<Table striped bordered hover responsive>
								<thead className="bg-dark text-white">
									<tr className="text-center">
										<th>First Name</th>
										<th>Middle Name</th>
										<th>Last Name</th>
										<th>Section</th>
										<th>Grade</th>
										<th>Sex</th>
										<th>Position</th>
										<th>Vote Count</th>
										<th colSpan='2' className="text-center">Actions</th>
										
									</tr>
								</thead>

								<tbody>
									
						   			{tre}
								</tbody>
							</Table>		
					  	</Tab>
						<Tab eventKey="auditor" title="Auditor" >
						   	<Table striped bordered hover responsive>
								<thead className="bg-dark text-white">
									<tr className="text-center">
										<th>First Name</th>
										<th>Middle Name</th>
										<th>Last Name</th>
										<th>Section</th>
										<th>Grade</th>
										<th>Sex</th>
										<th>Position</th>
										<th>Vote Count</th>
										<th colSpan='2' className="text-center">Actions</th>
										
									</tr>
								</thead>

								<tbody>
									{aud}
								</tbody>
							</Table>		
					  	</Tab>
						 <Tab eventKey="PIO" title="PIO" >
						 	<Table striped bordered hover responsive>
								<thead className="bg-dark text-white">
									<tr className="text-center">
										<th>First Name</th>
										<th>Middle Name</th>
										<th>Last Name</th>
										<th>Section</th>
										<th>Grade</th>
										<th>Sex</th>
										<th>Position</th>
										<th>Vote Count</th>
										<th colSpan='2' className="text-center">Actions</th>
										
									</tr>
								</thead>

								<tbody>
									{pio}
								</tbody>
							</Table>		
						   
					  	</Tab>
						 <Tab eventKey="po" title="Protocol Officer" >
						 	<Table striped bordered hover responsive>
								<thead className="bg-dark text-white">
									<tr className="text-center">
										<th>First Name</th>
										<th>Middle Name</th>
										<th>Last Name</th>
										<th>Section</th>
										<th>Grade</th>
										<th>Sex</th>
										<th>Position</th>
										<th>Vote Count</th>
										<th colSpan='2' className="text-center">Actions</th>
										
									</tr>
								</thead>

								<tbody>
									{po}
								</tbody>
							</Table>		
						</Tab>
						<Tab eventKey="GLR" title="Grade Level Representative">
						   	<Table striped bordered hover responsive>
								<thead className="bg-dark text-white">
									<tr className="text-center">
										<th>First Name</th>
										<th>Middle Name</th>
										<th>Last Name</th>
										<th>Section</th>
										<th>Grade</th>
										<th>Sex</th>
										<th>Position</th>
										<th>Vote Count</th>
										<th colSpan='2' className="text-center">Actions</th>
										
									</tr>
								</thead>
								<tbody>
									{glr}
								</tbody>
							</Table>		
					  	</Tab>

					  	<Tab eventKey="students" title="Students">
						   	<Table striped bordered hover responsive>
								<thead className="bg-dark text-white">
									<tr className="text-center">
										<th>LRN</th>
										<th>First Name</th>
										<th>Middle Name</th>
										<th>Last Name</th>
										<th>Section</th>
										<th>Grade</th>
										<th>Sex</th>
										<th colSpan='2' className="text-center">Actions</th>
										
									</tr>
								</thead>
								<tbody>
									{users}
								</tbody>
							</Table>		
					  	</Tab>
						
					</Tabs>
			</div>
	)

}
