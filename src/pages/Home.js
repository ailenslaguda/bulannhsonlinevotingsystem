import React, {useState, useEffect, useContext} from 'react';
import	{Col, Row, Form, Button} from "react-bootstrap";
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import {Navigate, useNavigate} from 'react-router-dom';
// import {Link} from 'react-router-dom';
// import Logout from './Logout'
export default function Login(){
	const {user, setUser} = useContext(UserContext);
	const navigate = useNavigate();
	// store values of input fields
	const [LRN, setLRN] = useState('');
	const [password, setPassword] = useState('');
	const [isActive, setIsActive] = useState(true);
	

	useEffect(()=>{
		if(LRN !=='' && password !== ''){
			setIsActive(true);
		} else{
			setIsActive(false);
		}
		
	}, [LRN, password])

	

	 // const routeChange = () =>{ 
	 //  	// navigate('/')
	 //  	Logout()
	 // }

	function loginUser(e) {
		e.preventDefault();

		// fetch('https://localhost:4000/users/login', {
		fetch('https://gnhs-ssg-online-voting-system.herokuapp.com/students/login', {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({
				_id: LRN,
				password: password
			})
		})
		.then(response => response.json())
		.then(data => {
			console.log(data.message)

			if (data.message === "Incorrect Password"){

				Swal.fire({
				  title: 'error',
				  text: `Incorrect Password`,
				  icon:'error'
				})
				navigate('/Home')

			}else if(data.message === "Student not found"){
				Swal.fire({
						  title: 'Error',
						  text: `Student not found. Register the student`,
						  icon:'error'
				})

				navigate('/home')
			} else if(data.message === "Student already voted"){
				Swal.fire({
						  title: 'Error',
						  text: `Student already voted.`,
						  icon:'error'
						})
				
				navigate('/home')
			} else{
				localStorage.setItem('accessToken', data.accessToken);
				localStorage.setItem('LRN', data._id);
				localStorage.setItem('firstName', data.firstName);
				localStorage.setItem('middleName', data.middleName);
				localStorage.setItem('lastName', data.lastName);
				localStorage.setItem('section', data.section);
				localStorage.setItem('grade', data.grade);

					
				setUser({accessToken: data.accessToken})

				if(data.isAdmin === true){
						localStorage.setItem('LRN', data._id);
						localStorage.setItem('isAdmin', data.isAdmin);
						localStorage.setItem('firstName', data.firstName);
						localStorage.setItem('middleName', data.middleName);
						localStorage.setItem('lastName', data.lastName);
						localStorage.setItem('section', data.section);
						setUser({
							LRN: data._id,
							isAdmin: data.isAdmin

						})

						setLRN('');
						setPassword('');

						Swal.fire({
						  title: 'Good job!',
						  text: `Login successful! Welcome Admin!`,
						  icon:'success'
						})

						navigate('/AdminView')

					}else {
						localStorage.setItem('LRN', data._id);
						localStorage.setItem('isAdmin', data.isAdmin);
						localStorage.setItem('grade', data.grade);
						localStorage.setItem('firstName', data.firstName);
						localStorage.setItem('middleName', data.middleName);
						localStorage.setItem('lastName', data.lastName);
						localStorage.setItem('section', data.section);
						setUser({
							LRN: data._id,
							isAdmin: data.isAdmin

						})
						// to be changed in the future
						setLRN('');
						setPassword('');

						Swal.fire({
						  title: 'Good job!',
						  text: `Login successful! Enjoy Voting`,
						  icon:'success'
						})

						navigate('/president')
					} 
			}

				
		})	

		// 	e.preventDefault();
		
	}
	const navToDashboard = () =>{
		<Navigate to='/president' />
	}

	return (
		(user.accessToken !== null) ?

			(user.isAdmin ===true)? 

			<Navigate to="/AdminView" />
			:
			<Navigate to="/president" />
		:
			<Row className="justify-content-md-center">
					
					<Col s={12} md={6}>
						
						<Form className = "p-1 m-5" onSubmit={(e) => loginUser(e) } onClick = {()=>navToDashboard()}>
							<Form.Group >
								<h1 className = "text-center mb-2"><strong>VOTING PORTAL</strong></h1>
								<Form.Label><strong>LRN</strong></Form.Label>
								<Form.Control 
									type="Number"
									placeholder = "123456789101"
									required	
									value={LRN}
									onChange={e => setLRN(e.target.value)}
								/>
							
							</Form.Group>

							<Form.Group className = "mt-3">
								<Form.Label><strong>PASSWORD</strong></Form.Label>
								<Form.Control 
									type="password"
									placeholder = "Enter password"
									required
									value={password}
									onChange={e => setPassword(e.target.value)}
								/>
							
							</Form.Group>
							
						
							{isActive ?
									<Row className="mt-3 p-3">
										<Button className="loginBut" type="submit" > LOGIN TO VOTE </Button>
								{/*		<Button className="mt-1" variant="secondary" onClick={()=>routeChange()}> Cancel </Button>*/}
									</Row>
									:
									<Row className="mt-3 p-3" >
										<Button className="loginBut" type="submit" disabled> LOGIN TO START VOTING </Button>
										{/*<Button className="mt-1" variant="secondary" onClick={()=>routeChange()}> Cancel </Button>*/}
									</Row>
								}
							{/*<p className="text-center"> Not yet a user? Ask your admin</p>*/}
							
							
						</Form>

						
					</Col>
					<Col s={12} md={6}> 
						<img className = "p-1 m-5" src="gnhs_logo.png" alt="gnhs_logo.png" width="70%"/>
					</Col>


				<Row>
					
					<br/>
					<br/>
					<br/>
					<br/>
					<br/>
					<br/>
					<br/>
					<br/>
					<br/>
					<br/>
					<br/>
					<br/>
					<br/>
					<br/>
					<br/>
					<br/>
					<br/>
					<br/>
					<br/>
					<br/>
				</Row>		
			</Row>
			
			
	)
}