import React, {useEffect, useContext} from 'react';
import {Navigate} from 'react-router-dom';
import UserContext from '../UserContext';

export default function Logout(){

	const {unsetUser, setUser} = useContext(UserContext);
	const usertoken = localStorage.getItem('accessToken')
	const isAdmin = localStorage.getItem('isAdmin')
	
	const doneVoting = ()=>{
		
		if (isAdmin === true){
			unsetUser();
			setUser({accessToken: null})
			localStorage.clear()
		}else{
			fetch(`https://gnhs-ssg-online-voting-system.herokuapp.com/students/student_update_activeStat`, {
			method: 'PUT',
			headers: {
				"Content-Type": 'application/json',
				"Authorization": `Bearer ${usertoken}`}
			})
			unsetUser();
			setUser({accessToken: null})
			localStorage.clear()
		}
		
	}

	

	useEffect(()=>{
		doneVoting()
		console.log(usertoken)
		
	}, [doneVoting])

	return(
		<>
			{/*<onLoad = {e=>doneVoting(e)} />*/}
			<Navigate to='/home' />
		</>
	)
}